﻿using NSubstitute;
using RomanNumberConverter;
using RomanNumberConverter.ToRomanCalculation;
using Xunit;

namespace RomanNumberConverterTest
{
    public class ConvertIntegerToRomanTest
    {
        [Fact]
        public void Given_A_Valid_Integer_Then_IntegerToRomanConversion_Is_Called_Once()
        {
            var calculateRomanNumber = Substitute.For<ICalculateRomanNumberFromAnInteger>();
            var convertIntegerToRoman = new ConvertIntegerToRoman(calculateRomanNumber);

            convertIntegerToRoman.Execute(1);

            calculateRomanNumber.Received(1).Execute(Arg.Is(1));
        }
    }
}
﻿using System;
using System.Linq;
using RomanNumberConverter.ToRomanCalculation;

namespace RomanNumberConverter.ToIntegerCalculation
{
    public class CalculateAnIntegerFromRoman : ICalculateAnIntegerFromRoman
    {
        private readonly IGetRomanNumeralsesOrdered _getRomanNumeralsesOrdered;

        public CalculateAnIntegerFromRoman() : this(new GetRomanNumeralsesOrdered())
        {
        }

        public CalculateAnIntegerFromRoman(IGetRomanNumeralsesOrdered getRomanNumeralsesOrdered)
        {
            _getRomanNumeralsesOrdered = getRomanNumeralsesOrdered;
        }

        public int Execute(string romanNumberToConvert)
        {
            var result = 0;

            while (romanNumberToConvert.Length > 0)
                foreach (var romanNumeral in _getRomanNumeralsesOrdered.Execute())
                {
                    if (romanNumberToConvert.StartsWith(romanNumeral.ToString()))
                    {
                        result = result + (int) romanNumeral;
                        romanNumberToConvert = romanNumberToConvert.Remove(0, romanNumeral.ToString().Length);
                        break;
                    }
                }

            return result;
        }
    }
}
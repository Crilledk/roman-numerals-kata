﻿namespace RomanNumberConverter.ToIntegerCalculation
{
    public interface ICalculateAnIntegerFromRoman
    {
        int Execute(string romanNumberToConvert);
    }
}
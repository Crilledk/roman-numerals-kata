namespace RomanNumberConverter.ToRomanCalculation
{
    public interface ICalculateRomanNumberFromAnInteger
    {
        string Execute(int numberToConvert);
    }
}
# README #

## Kort om opgaven ##
Kode Kata Romertal

Implementer en løsning, som kan konvertere romertal til decimalværdi og fra decimalværdi til romertal. 

### Jeg har valgt at fokusere på ###

* IoC  med Constructor injection
* UnitTest der er navngivet efter Given When Then og som fřlger Arrange Act Assert pattern. 
* Unit Tests er lavet ud fra sunshine examples filosofien.
* Jeg har brugt Poor mans dependency injection hvor det har vćret nřdvendigt.

### Jeg har fravalgt følgende grundet tidsaspektet ###

* At implementere en IoC Container. 
* At udfřrer validering og fejlhĺndtering 
* At lave BDD (SpecFlow) 

### Følgende teknologier er brugt ###

* xUnit
* FluentAssertion 
* NSubstitute
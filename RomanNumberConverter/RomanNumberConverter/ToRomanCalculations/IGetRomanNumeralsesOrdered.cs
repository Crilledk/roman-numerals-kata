using System.Linq;

namespace RomanNumberConverter.ToRomanCalculation
{
    public interface IGetRomanNumeralsesOrdered
    {
        IOrderedEnumerable<RomanNumerals> Execute();
    }
}
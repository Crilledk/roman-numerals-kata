﻿using RomanNumberConverter.ToIntegerCalculation;

namespace RomanNumberConverter
{
    public class ConvertRomanToInteger
    {
        private readonly ICalculateAnIntegerFromRoman _calculateIntegerFromRoman;

        public ConvertRomanToInteger(ICalculateAnIntegerFromRoman calculateIntegerFromRoman)
        {
            _calculateIntegerFromRoman = calculateIntegerFromRoman;
        }

        public int Execute(string romanNumberToConvert)
        {
            return _calculateIntegerFromRoman.Execute(romanNumberToConvert);
        }
    }
}
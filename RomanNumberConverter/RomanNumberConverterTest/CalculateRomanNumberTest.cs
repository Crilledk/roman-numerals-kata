﻿using System.Collections.Generic;
using FluentAssertions;
using RomanNumberConverter;
using RomanNumberConverter.ToRomanCalculation;
using Xunit;

namespace RomanNumberConverterTest
{
    public class CalculateRomanNumberTest
    {
        [Theory]
        [MemberData("SimpleRomanNumerals")]
        public void Given_An_Simple_Integer_When_ConvertingToRoman_Then_The_Result_Is_The_Correct_RomanNumeral(
            int integerToConvert, string expectedRomanNumerals)
        {
            var calculateRomanNumber = new CalculateRomanNumberFromAnInteger();

            var result = calculateRomanNumber.Execute(integerToConvert);

            result.Should().Be(expectedRomanNumerals);
        }

        public static IEnumerable<object[]> SimpleRomanNumerals()
        {
            yield return new object[]
            {
                1,
                RomanNumerals.I
            };
            yield return new object[]
            {
                5,
                RomanNumerals.V
            };
            yield return new object[]
            {
                10,
                RomanNumerals.X
            };
            yield return new object[]
            {
                50,
                RomanNumerals.L
            };
            yield return new object[]
            {
                100,
                RomanNumerals.C
            };
            yield return new object[]
            {
                500,
                RomanNumerals.D
            };
            yield return new object[]
            {
                1000,
                RomanNumerals.M
            };
        }

        [Fact]
        public void Given_1999_When_integerToRomanConversion_Then_The_Result_Is_MCMXCIX()
        {
            var calculateRomanNumber = new CalculateRomanNumberFromAnInteger();

            var result = calculateRomanNumber.Execute(1999);

            result.Should().Be("MCMXCIX");
        }

        [Fact]
        public void Given_2444_When_integerToRomanConversion_Then_The_Result_Is_MMCDXLIV()
        {
            var calculateRomanNumber = new CalculateRomanNumberFromAnInteger();

            var result = calculateRomanNumber.Execute(2444);

            result.Should().Be("MMCDXLIV");
        }

        [Fact]
        public void Given_90_When_integerToRomanConversion_Then_The_Result_Is_XC()
        {
            var calculateRomanNumber = new CalculateRomanNumberFromAnInteger();

            var result = calculateRomanNumber.Execute(90);

            result.Should().Be("XC");
        }

        [Fact]
        public void Given_3000_When_integerToRomanConversion_Then_The_Result_Is_MMM()
        {
            var calculateRomanNumber = new CalculateRomanNumberFromAnInteger();

            var result = calculateRomanNumber.Execute(3000);

            result.Should().Be("MMM");
        }
    }
}
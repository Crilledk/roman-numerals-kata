﻿using NSubstitute;
using RomanNumberConverter;
using RomanNumberConverter.ToIntegerCalculation;
using Xunit;

namespace RomanNumberConverterTest
{
    public class ConvertRomanToIntegerTest
    {
        [Fact]
        public void Given_A_Valid_RomanNumber_When_RomanToIntegerConversion_Then_Execute_Is_Called_Only_Once()
        {
            var calculateAnIntegerFromRoman = Substitute.For<ICalculateAnIntegerFromRoman>();
            var convertRomanToInteger = new ConvertRomanToInteger(calculateAnIntegerFromRoman);

            convertRomanToInteger.Execute("TEST");

            calculateAnIntegerFromRoman.Received(1).Execute(Arg.Is("TEST"));
        }
    }
}
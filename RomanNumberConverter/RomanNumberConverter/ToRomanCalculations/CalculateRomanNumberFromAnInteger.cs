using System;
using System.Linq;
using System.Text;

namespace RomanNumberConverter.ToRomanCalculation
{
    public class CalculateRomanNumberFromAnInteger : ICalculateRomanNumberFromAnInteger
    {
        private readonly IGetRomanNumeralsesOrdered _getRomanNumeralsesOrdered;

        public CalculateRomanNumberFromAnInteger() : this(new GetRomanNumeralsesOrdered())
        {
        }

        public CalculateRomanNumberFromAnInteger(IGetRomanNumeralsesOrdered getRomanNumeralsesOrdered)
        {
            _getRomanNumeralsesOrdered = getRomanNumeralsesOrdered;
        }

        public string Execute(int numberToConvert)
        {
            var romanNumber = new StringBuilder();

            while (numberToConvert > 0)
                foreach (var romanNumeral in _getRomanNumeralsesOrdered.Execute())
                {
                    if (numberToConvert >= (int) romanNumeral)
                    {
                        romanNumber = romanNumber.Append(romanNumeral);
                        numberToConvert = numberToConvert - (int) romanNumeral;
                        break;
                    }
                }

            return romanNumber.ToString();
        }
    }
}
﻿using System.Collections.Generic;
using FluentAssertions;
using RomanNumberConverter;
using RomanNumberConverter.ToIntegerCalculation;
using Xunit;

namespace RomanNumberConverterTest
{
    public class CalculateAnIntegerFromRomanTest
    {
        [Theory]
        [MemberData("SimpleRomanNumerals")]
        public void Given_An_Simple_RomanNumber_When_ConvertingToInteger_Then_The_Result_Is_The_Correct_Integer(
            string romanNumber, int expectedInteger)
        {
            var calculateAnIntegerFromRoman = new CalculateAnIntegerFromRoman();

            var result = calculateAnIntegerFromRoman.Execute(romanNumber);

            result.Should().Be(expectedInteger);
        }

        public static IEnumerable<object[]> SimpleRomanNumerals()
        {
            yield return new object[]
            {
                RomanNumerals.I,
                1
            };
            yield return new object[]
            {
                RomanNumerals.V,
                5
            };
            yield return new object[]
            {
                RomanNumerals.X,
                10
            };
            yield return new object[]
            {
                RomanNumerals.L,
                50
            };
            yield return new object[]
            {
                RomanNumerals.C,
                100
            };
            yield return new object[]
            {
                RomanNumerals.D,
                500
            };
            yield return new object[]
            {
                RomanNumerals.M,
                1000
            };
        }

        [Fact]
        public void Given_MCMXCIX_When_integerToRomanConversion_Then_The_Result_Is_1999()
        {
            var calculateAnIntegerFromRoman = new CalculateAnIntegerFromRoman();

            var result = calculateAnIntegerFromRoman.Execute("MCMXCIX");

            result.Should().Be(1999);
        }

        [Fact]
        public void Given_MMCDXLIV_When_integerToRomanConversion_Then_The_Result_Is_2444()
        {
            var calculateAnIntegerFromRoman = new CalculateAnIntegerFromRoman();
            ;

            var result = calculateAnIntegerFromRoman.Execute("MMCDXLIV");

            result.Should().Be(2444);
        }

        [Fact]
        public void Given_XC_When_integerToRomanConversion_Then_The_Result_Is_90()
        {
            var calculateAnIntegerFromRoman = new CalculateAnIntegerFromRoman();
            ;

            var result = calculateAnIntegerFromRoman.Execute("XC");

            result.Should().Be(90);
        }
    }
}
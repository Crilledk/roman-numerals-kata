using System;
using System.Linq;

namespace RomanNumberConverter.ToRomanCalculation
{
    public class GetRomanNumeralsesOrdered : IGetRomanNumeralsesOrdered
    {
        public IOrderedEnumerable<RomanNumerals> Execute()
        {
            return Enum.GetValues(typeof (RomanNumerals)).Cast<RomanNumerals>().OrderByDescending(x => x);
        }
    }
}
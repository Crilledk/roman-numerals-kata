﻿using RomanNumberConverter.ToRomanCalculation;

namespace RomanNumberConverter
{
    public class ConvertIntegerToRoman
    {
        private readonly ICalculateRomanNumberFromAnInteger _calculateRomanNumberFromAnInteger;

        public ConvertIntegerToRoman(ICalculateRomanNumberFromAnInteger calculateRomanNumberFromAnInteger)
        {
            _calculateRomanNumberFromAnInteger = calculateRomanNumberFromAnInteger;
        }

        public string Execute(int numberToConvert)
        {
            return _calculateRomanNumberFromAnInteger.Execute(numberToConvert);
        }
    }
}